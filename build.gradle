plugins {
    id 'com.github.sherter.google-java-format' version '0.8'
    id 'org.ajoberstar.grgit' version '4.0.1'
}

// Apply the java-library plugin to add support for Java Library
apply plugin: 'maven'
apply plugin: 'maven-publish'
apply plugin: 'signing'
apply plugin: 'idea'
apply plugin: 'eclipse'
apply plugin: 'java'
//apply plugin: 'war'
apply plugin: 'jacoco'

group 'link.luyu'
version '1.0.0'

repositories {
    jcenter()
    maven { url "https://oss.sonatype.org/content/repositories/snapshots" }
    maven { url 'http://maven.aliyun.com/nexus/content/groups/public/' }
    maven { url 'http://maven.aliyun.com/nexus/content/repositories/jcenter' }
}

dependencies {

    compile 'org.bouncycastle:bcprov-jdk15on:1.60'
    compile 'com.fasterxml.jackson.core:jackson-databind:2.11.2'

    // Use JUnit test framework
    testImplementation 'org.junit.jupiter:junit-jupiter-api:5.6.0'
    testRuntimeOnly 'org.junit.jupiter:junit-jupiter-engine'
    testCompileOnly 'junit:junit:4.13'
    testRuntimeOnly 'org.junit.vintage:junit-vintage-engine'
}

test {
    useJUnitPlatform()
}

googleJavaFormat {
    options style: 'AOSP'
    source = sourceSets*.allJava
    include '**/*.java'
    exclude '**/temp/*.java'
}

verifyGoogleJavaFormat {
    source = sourceSets*.allJava
    include '**/*.java'
    exclude '**/temp/*.java'
}

jar {
    destinationDir file('dist/apps')
    archiveName project.name + "-" + project.version + '.jar'
    exclude '**/*.xml'
    exclude '**/*.toml'
    exclude '**/*.properties'
    exclude '**/*.yml'
    exclude '**/*.crt'
    exclude '**/*.key'
    exclude '**/*.sql'
    exclude '**/*.pem'

    manifest {
        try {
            def repo = grgit.open(dir: file('.').canonicalPath)
            if (repo != null) {
                def date = new Date().format("yyyy-MM-dd'T'HH:mm:ssZ")
                def branch = repo.branch.getCurrent().getName()
                def commit = repo.head().getAbbreviatedId(40)

                attributes(["Implementation-Timestamp": date,
                            "Git-Branch"              : branch,
                            "Git-Commit"              : commit
                ])

                logger.info("  Commit : ")
                logger.info("  => date: {}", date)
                logger.info("  => branch: {}", branch)
                logger.info("  => commit: {}", commit)
            }
        } catch (Exception e) {
            // logger.warn(' .git not exist, cannot found commit info')
        }
    }

    doLast {
        copy {
            from file('src/main/resources/')
            into 'dist/conf'
        }
        copy {
            from configurations.runtime
            into 'dist/lib'
        }
        copy {
            from file('.').listFiles().findAll { File f -> (f.name.endsWith('.sh') || f.name.endsWith('.env')) }
            into 'dist'
        }
        copy {
            from file('scripts/')
            into 'dist'
        }
    }
}


ext {
    if (!project.hasProperty("NEXUS_USERNAME")) {
        NEXUS_USERNAME="xxx"
    }

    if (!project.hasProperty("NEXUS_PASSWORD")) {
        NEXUS_PASSWORD="xxx"
    }
}

task sourcesJar(type: Jar) {
    from sourceSets.main.allJava
    classifier = 'sources'
}

task javadocJar(type: Jar) {
    from javadoc
    classifier = 'javadoc'
}

javadoc {
    options {
        encoding 'UTF-8'
        charSet 'UTF-8'
    }
}

publishing {
    publications {
        mavenJava(MavenPublication) {
            groupId = group
            artifactId = project.name
            from components.java
            artifact sourcesJar
            artifact javadocJar

            // https://docs.gradle.org/current/dsl/org.gradle.api.publish.maven.MavenPublication.html
            pom {
                name = "LuyuProtocol"
                description = "Luyu Protocol"
                url = "https://gitee.com/luyu-community/luyu-cross-chain-protocol"
                licenses {
                    license {
                        name = "The Apache License, Version 2.0"
                        url = "http://www.apache.org/licenses/LICENSE-2.0.txt"
                    }
                }
                developers {
                    developer {
                        id = "Luyu"
                        name = "Luyu"
                        email = "luyu_protocol@163.com"
                    }
                }
                scm {
                    connection = "scm:git:git@://gitee.com:luyu-community/luyu-cross-chain-protocol.git"
                    developerConnection = "scm:git:ssh://gitee.com:luyu-community/luyu-cross-chain-protocol.git"
                    url = "https://gitee.com/luyu-community/luyu-cross-chain-protocol"
                }
            }
        }
    }
    repositories {
        maven {
            def releasesRepoUrl = "https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/"
            def snapshotsRepoUrl = "https://s01.oss.sonatype.org/content/repositories/snapshots/"
            url = version.endsWith('SNAPSHOT') ? snapshotsRepoUrl : releasesRepoUrl

            credentials {
                username = NEXUS_USERNAME
                password = NEXUS_PASSWORD
            }
        }
    }

    signing {
        sign publishing.publications.mavenJava
    }
}

test {
    useJUnitPlatform()
    testLogging.showStandardStreams = true
}

jacocoTestReport {
    reports {
        xml.enabled true
        html.enabled false
    }
}
